from __future__ import unicode_literals

from django.db import models
from swampdragon.models import SelfPublishModel
from .serializers import NotificationSerializer

from account.models import Account


class Message(SelfPublishModel, models.Model):

    class Meta:
        verbose_name_plural = 'Messages'
        ordering = ['-created']

    serializer_class = NotificationSerializer
    sender = models.ForeignKey(Account, related_name='messages_sent')
    recipient = models.ForeignKey(Account, related_name='messages_received')
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    unread = models.BooleanField(default=True)

    def mark_as_read(self, account):
        if self.recipient_id == account.id and self.unread:
            self.unread = False
            self.save()

    def __str__(self):
        return 'Message ' + str(self.id)

    def __unicode__(self):
        return 'Message ' + str(self.id)