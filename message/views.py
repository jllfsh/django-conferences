from django.shortcuts import redirect

from django.views.generic import DetailView, CreateView

from .models import Message


class ShowAllMessages(DetailView):

    template_name = 'messages.html'

    def get_object(self, queryset=None):
        return self.request.user


class ShowMessage(DetailView):

    template_name = 'message.html'

    def get_object(self, queryset=None):
        message = Message.objects.get(pk=self.kwargs['pk'])
        message.mark_as_read(self.request.user)
        return message


class NewMessage(CreateView):

    model = Message
    fields = ['text']
    template_name = 'new_message.html'

    def form_valid(self, form):
        message = form.save(commit=False)
        message.recipient_id = self.kwargs['recipient_id']
        message.sender_id = self.request.user.id
        message.save()

        message.recipient.email_user(
            'You have a new message',
            'Please, check your messages inbox',
        )

        return redirect('messages')