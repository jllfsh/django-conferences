from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from .views import ShowAllMessages, ShowMessage, NewMessage

urlpatterns = [
    url(r'^$', login_required(ShowAllMessages.as_view()), name='messages'),
    url(r'^(?P<pk>[0-9]+)$', login_required(ShowMessage.as_view()), name='show_message'),
    url(r'^new/(?P<recipient_id>[0-9]+)$', login_required(NewMessage.as_view()), name='new_message'),
]