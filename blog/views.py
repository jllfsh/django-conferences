from django.views.generic import ListView

from .models import News


class ShowAllNews(ListView):

    template_name = 'news.html'
    queryset = News.objects.all()
    paginate_by = 3
