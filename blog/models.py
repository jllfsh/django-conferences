from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from account.models import Account


class News(models.Model):

    class Meta:
        verbose_name_plural = 'News'
        ordering = ['-updated']

    topic = models.CharField(max_length=250)
    text = models.TextField()
    author = models.ForeignKey(Account, db_constraint=False, related_name='news_author', null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.topic

    def __unicode__(self):
        return self.topic
