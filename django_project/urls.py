from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required

from blog.views import ShowAllNews

urlpatterns = [
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'list_conferences'}, name='logout'),

    url(r'^admin/', admin.site.urls),

    url(r'^account/', include('account.urls')),
    url(r'^blog/', login_required(ShowAllNews.as_view()), name='show_all_news'),
    url(r'^conferences/', include('conferences.urls')),
    url(r'^messages/', include('message.urls')),
]
