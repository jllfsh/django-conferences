from __future__ import unicode_literals
from datetime import timedelta

from django.contrib.auth.models import User
from django.core.exceptions import SuspiciousOperation
from django.db import models
from django.utils import timezone

from account.models import Account


class Conference(models.Model):

    class Meta:
        verbose_name_plural = 'Conferences'

    name = models.CharField(max_length=100)
    theme = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    start_time = models.DateTimeField(default=timezone.now)
    end_time = models.DateTimeField(default=timezone.now)
    speakers = models.ManyToManyField(Account, related_name='conferences_speaker')
    participants = models.ManyToManyField(Account, related_name='conferences_participant')
    sponsors = models.ManyToManyField(Account, related_name='conferences_sponsor')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def is_over(self):
        return True if self.end_time > timezone.now() else False

    def is_register_available(self):
        return True if self.start_time > timezone.now() else False

    def add_listener(self, account):
        if not self.is_register_available():
            raise SuspiciousOperation('Register unavailable')

        if account in self.participants.all():
            raise SuspiciousOperation('You already have been registered')

        self.participants.add(account)

    @classmethod
    def get_current_conferences(cls):
        return cls.objects.filter(end_time__gte=timezone.now() - timedelta(days=3))

    @classmethod
    def get_archive_conferences(cls):
        return cls.objects.filter(end_time__lt=timezone.now() - timedelta(days=3))
