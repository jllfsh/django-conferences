from django.shortcuts import get_object_or_404, render
from django.views.generic import DetailView, ListView, View

from conferences.models import Account, Conference


class ShowAllConferences(ListView):
    template_name = 'all_conferences.html'
    queryset = Conference.get_current_conferences()
    paginate_by = 3


class ConferencesArchive(ListView):
    template_name = 'all_conferences.html'
    queryset = Conference.get_archive_conferences()
    paginate_by = 3


class ShowConference(DetailView):
    model = Conference
    template_name = 'show_conference.html'


class ShowConferenceListeners(DetailView):
    model = Conference
    template_name = 'show_conference_listeners.html'


class ConferenceRegistration(View):
    @staticmethod
    def get(request, pk):
        conference = get_object_or_404(Conference, pk=pk)
        listener = get_object_or_404(Account, pk=request.user.id)

        conference.add_listener(listener)

        return render(request, 'base.html', {'message': 'Registration completed'})
