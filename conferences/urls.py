from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from conferences.views import ShowAllConferences, ShowConference, ShowConferenceListeners, ConferencesArchive, \
    ConferenceRegistration

urlpatterns = [
    url(r'^$', ShowAllConferences.as_view(), name='list_conferences'),
    url(r'^archive/$', login_required(ConferencesArchive.as_view()), name='archive_conferences'),
    url(r'^(?P<pk>[0-9]+)/$', login_required(ShowConference.as_view()), name='conference_detail'),
    url(r'^(?P<pk>[0-9]+)/listeners/$', login_required(ShowConferenceListeners.as_view()), name='conference_listeners'),
    url(r'^(?P<pk>[0-9]+)/registration/$', login_required(ConferenceRegistration.as_view()),
        name='conference_registration'),
]
