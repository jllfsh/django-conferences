# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-09 09:26
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('conferences', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Company',
        ),
        migrations.AlterField(
            model_name='conference',
            name='sponsors',
            field=models.ManyToManyField(related_name='conferences_sponsor', to=settings.AUTH_USER_MODEL),
        ),
    ]
