from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required

from views import UserRegistration, EditUserProfile, ShowUserProfile


urlpatterns = [
    url(r'^(?P<pk>[0-9]+)$', login_required(ShowUserProfile.as_view()), name='show_account'),
    url(r'^(?P<pk>[0-9]+)/edit$', login_required(EditUserProfile.as_view()), name='edit_user_profile'),
    url(r'^password_change/$', auth_views.password_change, name='password_change'),
    url(r'^password_change/done/$', auth_views.password_change_done, name='password_change_done'),
    url(r'^registration/$', UserRegistration.as_view(), name='user_registration'),
]
