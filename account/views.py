from django.views.generic import CreateView, DetailView, UpdateView


from .forms import AccountCreationForm
from .models import Account


class UserRegistration(CreateView):
    template_name = 'registration.html'
    form_class = AccountCreationForm


class ShowUserProfile(DetailView):
    model = Account
    template_name = 'show_profile.html'

    def get_context_data(self, **kwargs):
        context = super(ShowUserProfile, self).get_context_data(**kwargs)
        account = context['account']
        context['is_owner'] = True if account.id == self.request.user.id else False
        return context


class EditUserProfile(UpdateView):
    model = Account
    fields = ['first_name', 'last_name', 'email', 'avatar']
    template_name = 'edit_profile.html'
