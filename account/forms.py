from django import forms
from django.contrib.auth import get_user_model


class AccountCreationForm(forms.ModelForm):

    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    password_confirm = forms.CharField(label='Password confirm', widget=forms.PasswordInput)

    def clean_password_confirm(self):
        password = self.cleaned_data.get('password')
        password_confirm = self.cleaned_data.get('password_confirm')
        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Passwords is not equal!')
        return password_confirm

    def save(self, commit=True):
        account = super(AccountCreationForm, self).save(commit=False)
        account.set_password(self.cleaned_data['password'])
        if commit:
            account.save()
        return account

    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name')
