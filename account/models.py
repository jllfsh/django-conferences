from __future__ import unicode_literals

from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import models


class AccountManager(BaseUserManager):

    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Email is required.')

        user = self.model(email=AccountManager.normalize_email(email),)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password)
        user.is_admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField('Email', max_length=255, unique=True, db_index=True)
    first_name = models.CharField('First name', max_length=40, null=True, blank=True)
    last_name = models.CharField('Last name', max_length=40, null=True, blank=True)
    avatar = models.ImageField('Avatar image', blank=True, null=True, upload_to="user/avatar")
    company = models.ForeignKey('self', related_name='representatives', blank=True, null=True,
                                on_delete=models.SET_NULL)
    register_date = models.DateField('Registration date', auto_now_add=True)
    is_active = models.BooleanField('Active', default=True)
    is_admin = models.BooleanField('Is superuser', default=False)
    is_company = models.BooleanField('Is company', default=False)

    def get_full_name(self):
        if self.first_name or self.last_name:
            full_name = "{first_name} {last_name}".format(first_name=self.first_name, last_name=self.last_name)
            return full_name.strip()

    @property
    def is_staff(self):
        return self.is_admin

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_absolute_url(self):
        return reverse('show_account', kwargs={'pk': self.pk})

    def __unicode__(self):
        return self.email

    def __str__(self):
        return self.email

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = AccountManager()

    class Meta:
        verbose_name = 'account'
        verbose_name_plural = 'accounts'
