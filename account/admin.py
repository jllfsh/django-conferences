from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group

from .forms import AccountCreationForm
from .models import Account


class AccountAdmin(UserAdmin):
    add_form = AccountCreationForm

    list_display = [
        'email',
        'first_name',
        'last_name',
        'is_admin',
        'is_company',
    ]

    list_filter = ('is_admin',)

    fieldsets = (
                (None, {'fields': ('email', 'password')}),
                ('Personal info', {
                 'fields': (
                     'avatar',
                     'first_name',
                     'last_name',
                 )}),
                ('Permissions', {'fields': ('is_admin', 'is_company',)}),
                ('Important dates', {'fields': ('last_login',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'email',
                'password',
                'password_confirm'
            )}
         ),
    )

    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

admin.site.register(Account, AccountAdmin)

admin.site.unregister(Group)
